package com.yihong.enums;

import lombok.Getter;

@Getter
public enum ConfigEnum {
	SYSTEM("系統設定", "system.properties");
	
	private String name;
	private String path;
	
	ConfigEnum (String name, String path) {
		this.name = name;
		this.path = path;
	}

}
