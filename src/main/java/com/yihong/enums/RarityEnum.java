package com.yihong.enums;

import lombok.Getter;

@Getter
public enum RarityEnum {
	NORMAL("NORMAL", "稀有度: 普通", 1),
	MAGIC("MAGIC", "稀有度: 魔法", 2),
	RARE("RARE", "稀有度: 稀有", 3),
	UNIQUE("UNIQUE", "稀有度: 傳奇", 4);
	
	private String name;
	private String keyWord;
	private int order;
	
	RarityEnum (String name, String keyWord, int order) {
		this.name = name;
		this.keyWord = keyWord;
		this.order = order;
	}

	public static RarityEnum getRarityEnum(String info) {
		if(info.indexOf(NORMAL.keyWord) >= 0) {
			return NORMAL;
		}else if(info.indexOf(MAGIC.keyWord) >= 0) {
			return MAGIC;
		}else if(info.indexOf(RARE.keyWord) >= 0) {
			return RARE;
		}else if(info.indexOf(UNIQUE.keyWord) >= 0) {
			return UNIQUE;
		}else {
			throw new UnknownError("無法解析該物品稀有度.");
		}
	}
	
	public static RarityEnum getRarityEnumByName(String name) {
		if(NORMAL.name.equals(name.trim())) {
			return NORMAL;
		}else if(MAGIC.name.equals(name.trim())) {
			return MAGIC;
		}else if(RARE.name.equals(name.trim())) {
			return RARE;
		}else if(UNIQUE.name.equals(name.trim())) {
			return UNIQUE;
		}else {
			throw new UnknownError("無法解析該物品稀有度.");
		}
	}
}
