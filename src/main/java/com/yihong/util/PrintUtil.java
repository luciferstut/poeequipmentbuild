package com.yihong.util;

import javax.swing.JTextArea;

import org.apache.log4j.Logger;

import com.yihong.exception.AppRuntimeException;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PrintUtil {
	private static PrintUtil instance;
	
	protected final JTextArea textArea = new JTextArea();
	
	private PrintUtil() { };
		
	
	public static PrintUtil getInstance() {
		if (instance == null) {
			instance = new PrintUtil();
		}

		return instance;
	}
	
	public void print(String msg) {
		textArea.append(msg);
		textArea.append("\n");
	}
	
	public void printWithLog(Logger log, String msg) {
		log.info(msg);
		print(msg);
	}
}
