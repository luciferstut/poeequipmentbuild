package com.yihong.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.yihong.enums.RarityEnum;
import com.yihong.vo.Item;
import com.yihong.vo.MatchResult;

import lombok.Getter;
import lombok.Setter;
@Setter
@Getter
public class AffixMatcher {
	
	public static Logger log = Logger.getLogger(AffixMatcher.class);
	
	PrintUtil printUtil =  PrintUtil.getInstance();
	
	private List<Pattern> patterns = new ArrayList<Pattern>();
	private int affixCount;
	private RarityEnum rarity;	
	
	public MatchResult match(Item item) {
		int matchCnt = 0;
		
		for (Pattern pattern : patterns) {
			for (String attr : item.getAttributes()) {
				//比對詞綴
				if (pattern.matcher(attr).find()) {
					printUtil.printWithLog(log, String.format("|| ********* 稀有度:%s 詞綴吻合 *********", rarity.getName()));
					matchCnt++;
					break;
				}
			}
		}
		
		boolean isDone = matchCnt >= affixCount;
		
		MatchResult result = new MatchResult();
				
		boolean reset = false;		
		if(!isDone
			&& item.getRarity().getOrder() >= rarity.getOrder()) {
			reset = true;
		}
		
		result.setMatchCnt(matchCnt);		
		result.setDone(isDone);
		result.setReset(reset);
		result.setTargetMatchCnt(affixCount);	
		return result;
	}
	
	public void clear() {
		rarity = null;
		affixCount = 0;
		patterns.clear();
	}
	
	public void addPatterns(Pattern pattern) {
		patterns.add(pattern);
	}
	
}
