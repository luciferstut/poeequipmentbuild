package com.yihong.util.factory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.yihong.enums.RarityEnum;
import com.yihong.exception.AppRuntimeException;
import com.yihong.util.AffixMatcher;
import com.yihong.util.PrintUtil;
import com.yihong.util.PropertiesUtil;

public class AffixMatcherFactory {
	public static Logger log = Logger.getLogger(AffixMatcherFactory.class);

	private final static String CNT_PARAM_KEY = "AffixMatcher.%s.targetAffixCnt";
	private final static String TARGET_PATTERNS_PARAM_KEY = "AffixMatcher.%s.targetPatterns";
		
	Properties properties = PropertiesUtil.getInstance();
	PrintUtil printUtil =  PrintUtil.getInstance();
	 
	private static AffixMatcherFactory instance;

	public static AffixMatcherFactory getInstance() {
		if (instance == null) {
			instance = new AffixMatcherFactory();
		}
		return instance;
	}
	
	public AffixMatcher createAffixMatcher(RarityEnum rarity) {
		AffixMatcher result = null;
		
		String enableRaritys = properties.getProperty("BaseGUI.enable.rarity");
		printUtil.printWithLog(log, "稀有度設定:" + enableRaritys);

		if (enableRaritys.toUpperCase().indexOf(rarity.getName()) >= 0) {

			printUtil.printWithLog(log, String.format("┌---------------- 讀取稀有度:[%s]設定 ----------------", rarity.getName()));
			result = new AffixMatcher();
			result.setRarity(rarity);
			result.setPatterns(loadPatterns(properties, rarity));
			result.setAffixCount(loadAffixCount(properties, rarity));

			printUtil.printWithLog(log, String.format("└---------------- 讀取稀有度:[%s]設定完成 ----------------", rarity.getName()));
			
		}
		
		return result;
	}

	private int loadAffixCount(Properties properties, RarityEnum rarity) {
		int affixCount = Integer
				.valueOf(properties.getProperty(String.format(CNT_PARAM_KEY, rarity.getName()), "2"));
		printUtil.printWithLog(log, String.format("目標詞綴數量: %s", affixCount));
		return affixCount;
	}
	
	private List<Pattern> loadPatterns(Properties properties, RarityEnum rarity) {
		String targetPatternString = properties
				.getProperty(String.format(TARGET_PATTERNS_PARAM_KEY, rarity.getName()));

		if (targetPatternString == null) {
			throw new AppRuntimeException("請設定目標詞綴於設定檔案(util.AffixMatcher.targetPatterns)，多個目標請使用逗號(,)分隔.");
		}

		List<Pattern> result = new ArrayList<Pattern>();
		
		String[] patterns = targetPatternString.split(",");

		for (String pattern : patterns) {
			printUtil.printWithLog(log, String.format("添加目標詞綴: %s", pattern));
			result.add(Pattern.compile(pattern.trim()));
		}
		
		return result;
	}
}
