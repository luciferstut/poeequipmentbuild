package com.yihong.util;

import org.apache.log4j.Logger;

import com.yihong.enums.RarityEnum;
import com.yihong.vo.Item;

public class ItemParser {

	public static Logger log = Logger.getLogger(ItemParser.class);
	
	private final static String SEPARATE_LINE = "--------\\n";

	public static Item Parser(String infoString) {
		return Parser(new Item(), infoString);
	}
	
	public static Item Parser(Item item, String infoString) {
		item.clear();
		
		item.setRarity(RarityEnum.getRarityEnum(infoString));
		
		//篩選掉用不到的資訊，方便抓取物品數值
		infoString = infoString.substring(infoString.indexOf("物品等級:"));
		
		String[] part = infoString.split(SEPARATE_LINE);
		int i = hasImplicit(infoString) ? 2 : 1;
		String[] infos = part[i].split("\\n");
		for (String info : infos) {
			log.debug(String.format("||---------%s", info));
			item.putAttribute(info);
		}
		return item;
	}

	private static boolean hasImplicit(String infoString) {
		return infoString.indexOf("(implicit)") >= 0 || infoString.indexOf("(enchant)") >= 0;
	}
}
