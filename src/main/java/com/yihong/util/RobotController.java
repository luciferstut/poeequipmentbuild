package com.yihong.util;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import com.yihong.exception.AppRuntimeException;
import com.yihong.grid.PositionGrid;

public class RobotController {
	private static RobotController instance;
	protected Robot robot;
	protected int delay = 150;
	
	private RobotController() throws AWTException {
		robot = new Robot();
	}

	public static RobotController getInstance(){
		try {
			if (instance == null) {
				instance = new RobotController();
			}
		}catch (AWTException e) {
			throw new AppRuntimeException(e);
		}
		return instance;
	}

	public void clickMouseLeftBtn() {
		robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
		robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
		robot.delay(delay);
	}

	public void clickMouseRigthLeftBtn() {
		robot.mousePress(InputEvent.BUTTON3_DOWN_MASK);
		robot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK);
		robot.delay(delay);
	}

	private void mouseMove(int x, int y) {
		robot.mouseMove(x, y);
		robot.delay(delay);
	}

	public void keyPress(int keycode) {
		robot.keyPress(keycode);
	}
	
	public void keyRelease(int keycode) {
		robot.keyPress(keycode);
	}
	
	public void keyPressCopy() {
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_C);
		robot.keyRelease(KeyEvent.VK_C);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		delay(delay);
	}

	public void mouseMove(PositionGrid grid) {
		mouseMove(grid.getX(), grid.getY());
	}

	public void delay(int ms) {
		robot.delay(ms);
	}
}
