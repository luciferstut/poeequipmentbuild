package com.yihong;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.Rectangle;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;

import org.apache.log4j.Logger;

import com.melloware.jintellitype.HotkeyListener;
import com.melloware.jintellitype.IntellitypeListener;
import com.melloware.jintellitype.JIntellitype;
import com.yihong.exception.AppRuntimeException;
import com.yihong.grid.uiconfig.TextField;
import com.yihong.util.PrintUtil;

public abstract class BaseGUI extends JFrame implements HotkeyListener, IntellitypeListener {

	public static Logger log = Logger.getLogger(BaseGUI.class);

	private static final long serialVersionUID = 1L;

	protected static final int F1 = 111;
	protected static final int F2 = 112;
	protected static final int F3 = 113;
	protected static final int F4 = 114;

	protected final JPanel mainPanel = new JPanel();
	protected final JPanel topPanel = new JPanel();
	protected final JPanel centerPanel = new JPanel();
	protected final JPanel bottomPanel = new JPanel();
	protected final JPanel leftPanel = new JPanel();

	protected final JScrollPane scrollPane = new JScrollPane();
	
	protected final JTextArea textArea = PrintUtil.getInstance().getTextArea();
	
	protected final JLabel coordinateX = new JLabel();
	protected final JLabel coordinateY = new JLabel();

	protected final TextField transOrbX = new TextField("grid.TransOrb.x");
	protected final TextField transOrbY = new TextField("grid.TransOrb.y");

	protected final TextField alterOrbX = new TextField("grid.AlterOrb.x");
	protected final TextField alterOrbY = new TextField("grid.AlterOrb.y");
	protected final TextField augmenOrbX = new TextField("grid.AugmenOrb.x");
	protected final TextField augmenOrbY = new TextField("grid.AugmenOrb.y");
	protected final TextField regalOrbX = new TextField("grid.RegalOrb.x");
	protected final TextField regalOrbY = new TextField("grid.RegalOrb.y");
	protected final TextField scouringOrbX = new TextField("grid.ScouringOrb.x");
	protected final TextField scouringOrbY = new TextField("grid.ScouringOrb.y");	
	protected final TextField buildItemX = new TextField("grid.BuildItem.x");
	protected final TextField buildItemY = new TextField("grid.BuildItem.y");
	
	protected TrayIcon trayIcon;
	protected SystemTray tray;

	public BaseGUI() {
		try {
			initComponents();
			initEvent();
		} catch (Exception e) {
			log.error(e, e);
		}
	}

	public void initJIntellitype() {
		try {
			// initialize JIntellitype with the frame so all windows commands can
			// be attached to this window
			JIntellitype.getInstance().addHotKeyListener(this);
			JIntellitype.getInstance().addIntellitypeListener(this);
//			output("JIntellitype initialized");
		} catch (AppRuntimeException ex) {
			log.error(ex);
		}
	}

	@Override
	public void onIntellitype(int command) {
		log.debug("onIntellitype");
	}

	public static void center(JFrame aFrame) {
		final GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		final Point centerPoint = ge.getCenterPoint();
		final Rectangle bounds = ge.getMaximumWindowBounds();
		final int w = Math.min(aFrame.getWidth(), bounds.width);
		final int h = Math.min(aFrame.getHeight(), bounds.height);
		final int x = centerPoint.x - (w / 2);
		final int y = centerPoint.y - (h / 2);
		aFrame.setBounds(x, y, w, h);
		if ((w == bounds.width) && (h == bounds.height)) {
			aFrame.setExtendedState(Frame.MAXIMIZED_BOTH);
		}
		aFrame.validate();
	}

	// 建立畫面
	private void initComponents() {
		initTopPanel();
		initCenterPanel();
		initBottomPanel();
		initLeftPanel();

		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(topPanel, BorderLayout.NORTH);
		mainPanel.add(centerPanel, BorderLayout.CENTER);
		mainPanel.add(bottomPanel, BorderLayout.SOUTH);

		mainPanel.add(leftPanel, BorderLayout.WEST);

		this.getContentPane().add(mainPanel);
		this.pack();
		this.setSize(800, 600);
	}

	private void initTopPanel() {

//		String [] a = {"身份證","軍人證","殘疾證","護照"};                //定義字串

//		jcombo.setSelectedIndex(2);

		topPanel.setBorder(new EtchedBorder(1));
//		topPanel.add(jcombo);
	}

	private void initLeftPanel() {
		leftPanel.setLayout(new GridLayout(6, 3));
		leftPanel.setBorder(new EtchedBorder(1));

		Object[][] gridInfos = { { "蛻變石:", transOrbX, transOrbY }, { "改造石:", alterOrbX, alterOrbY },
				{ "增幅石:", augmenOrbX, augmenOrbY }, { "富豪石:", regalOrbX, regalOrbY },
				{ "重鑄石:", scouringOrbX, scouringOrbY }, { "目標裝備座標:", buildItemX, buildItemY } };

		for (Object[] gridInfo : gridInfos) {
			JLabel label = new JLabel((String) gridInfo[0]);
			label.setFont(new Font("宋體", Font.PLAIN, 15));
			leftPanel.add(label);
			leftPanel.add((TextField) gridInfo[1]);
			leftPanel.add((TextField) gridInfo[2]);
		}
	}

	private void initCenterPanel() {
		centerPanel.setLayout(new BorderLayout());
		centerPanel.setBorder(new EtchedBorder(1));

		scrollPane.getViewport().add(textArea);
		centerPanel.add(scrollPane, BorderLayout.CENTER);
	}

	private void initBottomPanel() {
		JLabel lblx = new JLabel("座標x:");
		lblx.setFont(new Font("宋體", Font.PLAIN, 15));
		JLabel lbly = new JLabel("座標y:");
		lbly.setFont(new Font("宋體", Font.PLAIN, 15));

		coordinateX.setForeground(Color.BLUE);
		coordinateX.setFont(new Font("宋體", Font.PLAIN, 20));
		coordinateY.setForeground(Color.BLUE);
		coordinateY.setFont(new Font("宋體", Font.PLAIN, 20));

		bottomPanel.add(lblx);
		bottomPanel.add(coordinateX);
		bottomPanel.add(lbly);
		bottomPanel.add(coordinateY);
	}

	private void eanbleJIntellitype() {
		JIntellitype.getInstance().registerHotKey(F1, "F1");
		JIntellitype.getInstance().registerHotKey(F2, "F2");
		JIntellitype.getInstance().registerHotKey(F3, "F3");
		JIntellitype.getInstance().registerHotKey(F4, "F4");
	}

	private void initEvent() {
		eanbleJIntellitype();

		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent evt) {
				// don't forget to clean up any resources before close
				JIntellitype.getInstance().cleanUp();
				System.exit(0);
			}
		});

		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				Point point = java.awt.MouseInfo.getPointerInfo().getLocation();
				coordinateX.setText("" + point.x);
				coordinateY.setText("" + point.y);
			}
		}, 100, 100);

		enableHideToTray();
	}

	private void enableHideToTray(){
		if (SystemTray.isSupported()) {
			tray = SystemTray.getSystemTray();

			Image image = null;
			try {
				image = ImageIO.read(getClass().getClassLoader().getResource("image/icon.jpg"));
			} catch (IOException e1) {
				log.error(e1);
			}
			
			ActionListener exitListener = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					log.debug("Exiting....");
					System.exit(0);
				}
			};
			
			PopupMenu popup = new PopupMenu();
			MenuItem defaultItem = new MenuItem("Exit");
			defaultItem.addActionListener(exitListener);
			popup.add(defaultItem);
			defaultItem = new MenuItem("Open");
			defaultItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setVisible(true);
					setExtendedState(JFrame.NORMAL);
				}
			});
			popup.add(defaultItem);
			trayIcon = new TrayIcon(image, "SystemTray Demo", popup);
			trayIcon.setImageAutoSize(true);
		} else {
			log.warn("system tray not supported");
		}
		addWindowStateListener(new WindowStateListener() {
			public void windowStateChanged(WindowEvent e) {
				if (e.getNewState() == ICONIFIED) {
					try {
						tray.add(trayIcon);
						setVisible(false);
						log.debug("added to SystemTray-1");
					} catch (AWTException ex) {
						log.debug("unable to add to tray");
					}
				}
				if (e.getNewState() == 7) {
					try {
						tray.add(trayIcon);
						setVisible(false);
						log.debug("added to SystemTray-2");
					} catch (AWTException ex) {
						log.debug("unable to add to system tray");
					}
				}
				if (e.getNewState() == MAXIMIZED_BOTH) {
					tray.remove(trayIcon);
					setVisible(true);
					log.debug("Tray icon removed");
				}
				if (e.getNewState() == NORMAL) {
					tray.remove(trayIcon);
					setVisible(true);
					log.debug("Tray icon removed");
				}
			}
		});
//		setIconImage(Toolkit.getDefaultToolkit().getImage("Duke256.png"));
	}

	// 接收按鈕事件
	public void onHotKey(int aIdentifier) {
		log.debug("WM_HOTKEY message received " + Integer.toString(aIdentifier));

		try {
			if (F1 == aIdentifier) {
				F1Event();
			}else if (F2 == aIdentifier) {
				F2Event();
			} else if (F3 == aIdentifier) {
				F3Event();
			} else if (F4 == aIdentifier) {
				F4Event();
			}

		} catch (Exception e) {
			log.error(e, e);
		}
	}

	abstract void F1Event();
	
	abstract void F2Event();

	abstract void F3Event();

	abstract void F4Event();

}
