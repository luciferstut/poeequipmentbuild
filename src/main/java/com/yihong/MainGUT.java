package com.yihong;

import java.awt.AWTException;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;

import com.yihong.enums.RarityEnum;
import com.yihong.exception.AppRuntimeException;
import com.yihong.service.BulidService;
import com.yihong.util.AffixMatcher;
import com.yihong.util.PropertiesUtil;
import com.yihong.util.factory.AffixMatcherFactory;

public class MainGUT extends BaseGUI {

	public static Logger log = Logger.getLogger(MainGUT.class);

	private static final long serialVersionUID = 1L;

	BuildJobThread buildJobThread = null;

	public static void main(String[] args) throws AWTException {
		MainGUT mainFrame = new MainGUT();
		mainFrame.setTitle("JIntellitype Test Application");
		MainGUT.center(mainFrame);
		mainFrame.setVisible(true);
		mainFrame.initJIntellitype();
	}

	@Override
	void F1Event() {
		try {
			PropertiesUtil.getInstance().store();
		} catch (IOException | URISyntaxException e) {
			new AppRuntimeException("儲存失敗..", e);
		}
	}
	
	@Override
	void F2Event() {
		PropertiesUtil.getInstance().reload();
		buildJobThread = new BuildJobThread();
		buildJobThread.start();
	}

	@SuppressWarnings("deprecation")
	@Override
	void F3Event() {
		buildJobThread.stop();
	}

	@Override
	void F4Event() {
		try {
			if (isVisible()) {
				tray.add(trayIcon);
			} else {
				tray.remove(trayIcon);
			}
			setVisible(!isVisible());
		} catch (AWTException e) {
			throw new AppRuntimeException(e);
		}
	}
}

class BuildJobThread extends Thread {

	public Logger log = Logger.getLogger(getClass());
	
	private BulidService service;

	private AffixMatcher magicMatcher = AffixMatcherFactory.getInstance().createAffixMatcher(RarityEnum.MAGIC);
	private AffixMatcher rareMatcher = AffixMatcherFactory.getInstance().createAffixMatcher(RarityEnum.RARE);;

	public BuildJobThread() {
		service = new BulidService(magicMatcher, rareMatcher);
	}

	public void run() {
		service.excute();
	}

}
