package com.yihong.service;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.yihong.enums.RarityEnum;
import com.yihong.exception.AppRuntimeException;
import com.yihong.grid.PositionGrid;
import com.yihong.grid.factory.GridFactory;
import com.yihong.grid.position.AlterOrb;
import com.yihong.grid.position.AugmenOrb;
import com.yihong.grid.position.BuildItem;
import com.yihong.grid.position.RegalOrb;
import com.yihong.grid.position.ScouringOrb;
import com.yihong.grid.position.TransOrb;
import com.yihong.util.AffixMatcher;
import com.yihong.util.ItemParser;
import com.yihong.util.RobotController;
import com.yihong.vo.BulidResult;
import com.yihong.vo.Item;
import com.yihong.vo.MatchResult;

public class BulidService {

	public Logger log = Logger.getLogger(getClass());

	private static RobotController robot;

	protected AffixMatcher magicMatcher;
	protected AffixMatcher rareMatcher;

	protected Item item = new Item();
	protected final PositionGrid transOrb = GridFactory.getInstance().createGrid(TransOrb.class);
	protected final PositionGrid alterOrb = GridFactory.getInstance().createGrid(AlterOrb.class);
	protected final PositionGrid augmenOrb = GridFactory.getInstance().createGrid(AugmenOrb.class);
	protected final PositionGrid alchemyOrb = GridFactory.getInstance().createGrid(RegalOrb.class);
	protected final PositionGrid scouringOrb = GridFactory.getInstance().createGrid(ScouringOrb.class);
	protected final PositionGrid buildItem = GridFactory.getInstance().createGrid(BuildItem.class);

	private BulidResult bulidResult;

	public BulidService(AffixMatcher magicMatcher, AffixMatcher rareMatcher) {
		super();
		robot = RobotController.getInstance();
		this.magicMatcher = magicMatcher;
		this.rareMatcher = rareMatcher;
	}

	public BulidResult excute() {		

		if(magicMatcher == null) {
			throw new AppRuntimeException("至少需要AffixMatcher.MAGIC.targetAffixCnt與AffixMatcher.MAGIC.targetPatterns兩項參數.");
		}
		
		bulidResult = new BulidResult();
		
		parseItem();
		int executeCnt = 1;
		while (true) {	
			log.debug(String.format("┌--------- 洗詞綴 第%s次 ---------", executeCnt));				
			if (doBuilding()) {
				log.info("************** 賓果拉 **************");
				bulidResult.setSuccess(true);
				break;
			}
			executeCnt++;
			log.debug("└--------- 洗詞綴 結束 ---------");
		}
		
		bulidResult.setExecuteCnt(executeCnt);
		
		return bulidResult;
	}

	private boolean doBuilding() {
		parseItem();
		MatchResult magicMatchResult = magicMatcher.match(item);
		MatchResult rareMatchResult = rareMatcher != null ? rareMatcher.match(item) : null;

		log.debug("物品稀有度:" + item.getRarity().getName());
		log.debug("物品詞綴數量:" + item.getAttributes().size());
		if (item.getRarity() == RarityEnum.NORMAL) {// 普通
			// 蛻變
			normalToMagic();

		} else if (item.getRarity() == RarityEnum.MAGIC) {// 魔法

			// 尚未達標，且僅未詞綴
			if (item.getAttributes().size() < 2 && (magicMatchResult.getTargetMatchCnt() < 2 || (magicMatchResult.getTargetMatchCnt() == 2 && magicMatchResult.getMatchCnt() == 1))) {
				log.debug("使用增幅");
				// 增幅
				useAugmenOrb();
			} else if (magicMatchResult.isDone() && rareMatchResult != null) {
				// 富豪
				MagicToRare();
			} else if (!magicMatchResult.isDone()) { // 滿詞 且未達標
				log.debug("使用改造");
				// 改造
				useAlterOrb();
			}

		} else if (item.getRarity() == RarityEnum.RARE) {// 稀有
			if (rareMatchResult == null || !rareMatchResult.isDone()) {
				// 重鑄
				useScouringOrb();
			}
		}

		log.debug("magicMatchResult.isDone():" + magicMatchResult.isDone());

		return (magicMatchResult.isDone()
				&& (rareMatchResult == null || (rareMatchResult != null && rareMatchResult.isDone())));
	}

	private void normalToMagic() {
		robot.mouseMove(transOrb);
		robot.clickMouseRigthLeftBtn();
		robot.mouseMove(buildItem);
		robot.clickMouseLeftBtn();

		bulidResult.addCnt(transOrb);
	}

	private void useAlterOrb() {
		robot.mouseMove(alterOrb);
		robot.clickMouseRigthLeftBtn();		
		robot.mouseMove(buildItem);
		robot.clickMouseLeftBtn();		
		bulidResult.addCnt(alterOrb);
	}

	private void useAugmenOrb() {
		robot.mouseMove(augmenOrb);
		robot.clickMouseRigthLeftBtn();
		robot.mouseMove(buildItem);
		robot.clickMouseLeftBtn();

		bulidResult.addCnt(augmenOrb);
	}

	private void MagicToRare() {
		robot.mouseMove(alchemyOrb);
		robot.clickMouseRigthLeftBtn();
		robot.mouseMove(buildItem);
		robot.clickMouseLeftBtn();

		bulidResult.addCnt(alchemyOrb);
	}

	private void useScouringOrb() {
		robot.mouseMove(scouringOrb);
		robot.clickMouseRigthLeftBtn();
		robot.mouseMove(buildItem);
		robot.clickMouseLeftBtn();

		bulidResult.addCnt(scouringOrb);
	}

	private void parseItem() {
		robot.mouseMove(buildItem);
		robot.keyPressCopy();
		ItemParser.Parser(this.item, getItemInfoString());
	}

	private String getItemInfoString() {
		Transferable t = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
		try {
			if (t != null && t.isDataFlavorSupported(DataFlavor.stringFlavor)) {
				// 因為原系的剪貼簿裡有多種資訊, 如文字, 圖片, 檔案等
				// 先判斷開始取得的可傳輸的資料是不是文字, 如果是, 取得這些文字
				return (String) t.getTransferData(DataFlavor.stringFlavor);
			} else {
				throw new AppRuntimeException("取得物品資訊時發生錯誤.");
			}
		} catch (UnsupportedFlavorException ex) {
			throw new AppRuntimeException(ex);
		} catch (IOException ex) {
			throw new AppRuntimeException(ex);
		}
	}
}
