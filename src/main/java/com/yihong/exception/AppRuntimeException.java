package com.yihong.exception;

public class AppRuntimeException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;

	public AppRuntimeException(){
		super();
	}
	
	public AppRuntimeException(String msg){
		super(msg);
	}
	
	public AppRuntimeException(Exception e){
		super(e);
	}
	
	public AppRuntimeException(String msg, Exception e){
		super(msg, e);
	}
}

