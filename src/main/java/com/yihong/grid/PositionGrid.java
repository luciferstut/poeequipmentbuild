package com.yihong.grid;

public class PositionGrid {
	protected int x;
	protected int y;
		
	public PositionGrid(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}	
	public int getY() {
		return y;
	}
}
