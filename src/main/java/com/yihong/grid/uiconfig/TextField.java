package com.yihong.grid.uiconfig;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

import com.yihong.enums.ConfigEnum;
import com.yihong.exception.AppRuntimeException;
import com.yihong.util.PropertiesUtil;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TextField extends JTextField{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected ConfigEnum config;
	protected String paramName;
	
	public TextField(String paramName) {
		super();
		this.config = ConfigEnum.SYSTEM;
		this.paramName = paramName;
		this.setText(getValueForConfig());
		this.getDocument().addDocumentListener(new DocumentListener(){
		   
			public void changedUpdate(DocumentEvent event){
				try {
					setValueToConfig(event.getDocument().getText(0, event.getDocument().getLength()));
				} catch (BadLocationException | IOException | URISyntaxException e) {
					throw new AppRuntimeException("儲存失敗.", e);
				}
		    }

			@Override
			public void insertUpdate(DocumentEvent e) {
				changedUpdate(e);
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				changedUpdate(e);
			};
		});
	}
	

	public String getValueForUI() {
		return this.getText();
	}
	
	public String getValueForConfig() {
		return PropertiesUtil.getInstance(config).getProperty(paramName);
	}
	
	public void setValueToConfig(String value) throws IOException, URISyntaxException {
		PropertiesUtil.getInstance(config).setProperty(paramName, value);
	}
	
}
