package com.yihong.grid.factory;

import java.util.Properties;

import com.yihong.exception.AppRuntimeException;
import com.yihong.grid.PositionGrid;
import com.yihong.util.PropertiesUtil;

public class GridFactory {
	private static GridFactory instance;
	protected Properties properties = PropertiesUtil.getInstance();

	private final static String POSITION_X_KEY = "grid.%s.x";
	private final static String POSITION_Y_KEY = "grid.%s.y";

	private GridFactory() {
	};


	public static GridFactory getInstance() {
		if (instance == null) {
			instance = new GridFactory();
		}

		return instance;
	}

	public PositionGrid createGrid(Class<? extends PositionGrid> clazz) {
		try {
			int x = Integer.valueOf(properties.getProperty(String.format(POSITION_X_KEY, clazz.getSimpleName())));
			int y = Integer.valueOf(properties.getProperty(String.format(POSITION_Y_KEY, clazz.getSimpleName())));
			return (PositionGrid) clazz.getConstructor(int.class, int.class).newInstance(x, y);
		} catch (Exception e) {
			throw new AppRuntimeException("createGrid時發生錯誤.", e);
		}
	}
}
