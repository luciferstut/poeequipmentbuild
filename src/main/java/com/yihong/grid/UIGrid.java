package com.yihong.grid;

import com.yihong.enums.ConfigEnum;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UIGrid{
	
	protected ConfigEnum config;
	protected String paramName;
	
	
}
