package com.yihong.vo;

import java.util.HashMap;
import java.util.Map;

import com.yihong.grid.PositionGrid;

import lombok.Data;

@Data
public class BulidResult {
	private boolean success = false;
	private long executeCnt;
	private Map<PositionGrid, Long> gridUseCounter = new HashMap<PositionGrid, Long>();

	public void addCnt(PositionGrid grid) {
		if (gridUseCounter.containsKey(grid)) {
			gridUseCounter.put(grid, gridUseCounter.get(grid) + 1);
		} else {
			gridUseCounter.put(grid, 1l);
		}
	}
}
