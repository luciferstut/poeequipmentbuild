package com.yihong.vo;

import java.util.ArrayList;
import java.util.List;

import com.yihong.enums.RarityEnum;

import lombok.Data;

@Data
public class Item {
	private RarityEnum rarity;
	private List<String> attributes = new ArrayList<String>();
	
	public void putAttribute(String attribute) {
		attributes.add(attribute);
	}
	
	public void clear() {
		this.rarity = null;
		this.attributes.clear();
	}
}