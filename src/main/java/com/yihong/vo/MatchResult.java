package com.yihong.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MatchResult {
	private boolean done;
	private int targetMatchCnt;
	private int matchCnt;
	private boolean reset;
}
